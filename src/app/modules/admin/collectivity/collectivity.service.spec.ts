import { TestBed } from '@angular/core/testing';

import { CollectivityService } from './collectivity.service';

describe('CollectivityService', () => {
  let service: CollectivityService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CollectivityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
