import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, Routes} from '@angular/router';
import {catchError, throwError} from "rxjs";
import {inject} from "@angular/core";
import {CollectivityComponent} from "./collectivity.component";
import {list} from "postcss";
import {ListComponent} from "./list/list.component";
import {CollectivityService} from "./collectivity.service";
import {SaveComponent} from "./save/save.component";
import {DetailsComponent} from "./details/details.component";


export default [
    {
        path: '',
        component: CollectivityComponent,
        children: [
            {
                path: '',
                pathMatch: 'full',
                component: ListComponent,
                resolve  : {
                    collectivities: () => inject(CollectivityService).getCollectivity(),
                },
            },
            {
                path: 'create',
                pathMatch: 'full',
                component: SaveComponent,

            },
            {
                path: 'details/:id',
                pathMatch: 'full',
                component: DetailsComponent,
            }
            // {
            //     path     : ':id',
            //     component: UsersDetailsComponent,
            //     resolve  : {
            //         user: userResolver,
            //     }
            // }
        ]
    }
] as Routes;
