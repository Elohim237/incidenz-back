export interface Collectivity {
    name?: string;
    email?: string;
    tel?: string;
    adresse?: string;
}
