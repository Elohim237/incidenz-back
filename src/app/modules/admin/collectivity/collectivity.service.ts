import { Injectable } from '@angular/core';
import {Collectivity} from "./collectivity.type";
import {BehaviorSubject, Observable, of, switchMap, tap} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Pagination} from "../../../core/pagination/pagination.type";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CollectivityService {

    private path = 'collectivity';
    private _collectivity: BehaviorSubject<Collectivity | null> = new BehaviorSubject(null);
    private _collectivities: BehaviorSubject<Collectivity[] | null> = new BehaviorSubject(null);
    private _pagination: BehaviorSubject<any> = new BehaviorSubject(null);

    constructor(private _httpClient: HttpClient)
    {
    }

    get collectivities$(): Observable<Collectivity[]>
    {
        return this._collectivities.asObservable();
    }

    get pagination$(): Observable<any>
    {
        return this._pagination.asObservable();
    }



    getCollectivity(page: number = 1, size: number = 10, sort: string = 'name', order: 'asc' | 'desc' | '' = 'asc', search: string = ''): Observable<Collectivity[]>
    {
        page = page === 0 ? 1 : page;
        return this._httpClient.get<{pagination: Pagination; collectivities: Collectivity[]}>(environment.api + this.path + '/all-pagination/', {
            params: {
                page: '' + page,
                limit: size,
                sort,
                order,
                search,
            }
        }).pipe(
            tap((response: any) => {
                this._pagination.next(response.pagination);
                this._collectivities.next(response.collectivities);
            }),
        );
    }

    createCommunity(data: any): Observable<any> {
        return this._httpClient.post(environment.api + this.path + '/create', data).pipe(
            switchMap((data: any) =>
            {
                this._collectivity = data;

                return of(data);
            }),
        )
    }

}
