import { Injectable } from '@angular/core';
import {TypeIncident} from "./type-incident";
import {BehaviorSubject, Observable, of, switchMap, tap} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Pagination} from "../../../core/pagination/pagination.type";
import {environment} from "../../../../environments/environment";
import {Collectivity} from "../collectivity/collectivity.type";

@Injectable({
  providedIn: 'root'
})
export class TypeIncidentService {

    private path = 'typeIncident';
    private _typeIncident: BehaviorSubject<TypeIncident | null> = new BehaviorSubject(null);
    private _typeIncidents: BehaviorSubject<TypeIncident[] | null> = new BehaviorSubject(null);
    private _pagination: BehaviorSubject<any> = new BehaviorSubject(null);
    constructor(private _httpClient: HttpClient) { }

    get typeIncidents$(): Observable<Collectivity[]>
    {
        return this._typeIncidents.asObservable();
    }

    get pagination$(): Observable<any>
    {
        return this._pagination.asObservable();
    }



    getTypeIncident(page: number = 1, size: number = 10, sort: string = 'name', order: 'asc' | 'desc' | '' = 'asc', search: string = ''): Observable<TypeIncident[]>
    {
        page = page === 0 ? 1 : page;
        return this._httpClient.get<{pagination: Pagination; typeIncidents: TypeIncident[]}>(environment.api + this.path + '/all-pagination', {
            params: {
                page: '' + page,
                limit: size,
                sort,
                order,
                search,
            }
        }).pipe(
            tap((response: any) => {
                this._pagination.next(response.pagination);
                this._typeIncidents.next(response.typeIncidents);
            }),
        );
    }

    createtypeIncident(data: any): Observable<any> {
        return this._httpClient.post(environment.api + this.path + '/create', data).pipe(
            switchMap((data: any) =>
            {
                this._typeIncident = data;

                return of(data);
            }),
        )
    }
}
