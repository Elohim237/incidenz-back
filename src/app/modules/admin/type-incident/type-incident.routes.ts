import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, Routes} from '@angular/router';
import {catchError, throwError} from "rxjs";
import {inject} from "@angular/core";
import {list} from "postcss";
import {ListComponent} from "./list/list.component";
import {SaveComponent} from "./save/save.component";
import {DetailsComponent} from "./details/details.component";
import {TypeIncidentComponent} from "./type-incident.component";
import {TypeIncidentService} from "./type-incident.service";


export default [
    {
        path: '',
        component: TypeIncidentComponent,
        children: [
            {
                path: '',
                pathMatch: 'full',
                component: ListComponent,
                resolve  : {
                    typesIncident: () => inject(TypeIncidentService).getTypeIncident(),
                },
            },
            {
                path: 'create',
                pathMatch: 'full',
                component: SaveComponent,

            },
            {
                path: 'details/:id',
                pathMatch: 'full',
                component: DetailsComponent,
            }
            // {
            //     path     : ':id',
            //     component: UsersDetailsComponent,
            //     resolve  : {
            //         user: userResolver,
            //     }
            // }
        ]
    }
] as Routes;
