import {AfterViewInit, Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {
    FormControl,
    FormsModule,
    ReactiveFormsModule,
    UntypedFormBuilder,
    UntypedFormGroup,
    Validators
} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatOptionModule, ThemePalette} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatStepperModule} from '@angular/material/stepper';
import {Color, NgxMatColorPickerModule} from '@angular-material-components/color-picker';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {NgFor, NgIf} from "@angular/common";
import {FuseAlertComponent, FuseAlertType} from "../../../../../@fuse/components/alert";
import {ActivatedRoute, Router} from "@angular/router";
import {TypeIncidentService} from "../type-incident.service";
import {data} from "autoprefixer";
@Component({
    selector: 'app-save',
    templateUrl: './save.component.html',
    styleUrls: ['./save.component.scss'],
    encapsulation: ViewEncapsulation.None,
    standalone: true,
    imports: [MatIconModule,
        FormsModule, ReactiveFormsModule, FuseAlertComponent, NgxMatColorPickerModule, MatStepperModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatOptionModule, MatButtonModule, MatCheckboxModule, MatRadioModule, MatProgressSpinnerModule, NgIf, NgFor],

})
export class SaveComponent implements OnInit, AfterViewInit {
    horizontalStepperForm: UntypedFormGroup;

    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: '',
    };
    showAlert: boolean = false;

    public disabled = false;
    color: ThemePalette = 'primary';
    public touchUi = false;

    constructor(private _formBuilder: UntypedFormBuilder,
                private _activatedRoute: ActivatedRoute,
                private _router: Router,
                private _typeIncidentService: TypeIncidentService) {
    }

    ngAfterViewInit(): void {
    }

    ngOnInit(): void {
        // Horizontal stepper form
        this.horizontalStepperForm = this._formBuilder.group({
            community: this._formBuilder.group({
                name: ['', Validators.required],
                address: ['', Validators.required],
                tel: ['', Validators.required],
                email: ['', [Validators.required, Validators.email]],
            }),
            manager: this._formBuilder.group({
                first_name: ['', Validators.required],
                last_name: ['', Validators.required],
                tel: ['', Validators.required],
                email: ['', [Validators.required, Validators.email]],

            }),
        });
    }

    submit(): void {
        // Return if the form is invalid
        if (this.horizontalStepperForm.invalid) {
            return;
        }

        // Disable the form
        this.horizontalStepperForm.disable();

        // Hide the alert
        this.showAlert = false;

        this._typeIncidentService.createtypeIncident(this.horizontalStepperForm.value).subscribe(
            () => {
                // Set the redirect url.
                // The '/signed-in-redirect' is a dummy url to catch the request and redirect the user
                // to the correct page after a successful sign in. This way, that url can be set via
                // routing file and we don't have to touch here.
                const redirectURL = this._activatedRoute.snapshot.queryParamMap.get('redirectURL') || '/signed-in-redirect';

                // Navigate to the redirect url
                this._router.navigateByUrl(redirectURL);
            },
            (response) => {
                console.log(response);
                // Re-enable the form
                this.horizontalStepperForm.enable();

                // Set the alert
                this.alert = {
                    type: 'error',
                    message: 'Unable to create company',
                };

                // Show the alert
                this.showAlert = true;
            },
        )
    }

}
