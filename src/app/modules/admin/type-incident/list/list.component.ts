import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CdkScrollable} from "@angular/cdk/scrolling";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSort, MatSortModule, Sort} from "@angular/material/sort";
import {MatTableModule} from "@angular/material/table";
import {MatPaginator, MatPaginatorModule} from "@angular/material/paginator";
import {MatSelectModule} from "@angular/material/select";
import {MatOptionModule} from "@angular/material/core";
import {AsyncPipe, I18nPluralPipe, NgClass, NgFor, NgIf, PercentPipe} from "@angular/common";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatButtonModule} from "@angular/material/button";
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {ReactiveFormsModule, UntypedFormControl} from "@angular/forms";
import {Pagination} from "../../../../core/pagination/pagination.type";
import {map, merge, Observable, Subject, switchMap, takeUntil} from "rxjs";
import {TypeIncident} from "../type-incident";
import {LiveAnnouncer} from "@angular/cdk/a11y";
import {TypeIncidentService} from "../type-incident.service";

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    standalone: true,
    imports: [CdkScrollable, MatFormFieldModule, MatSortModule, MatTableModule, MatPaginatorModule, MatSelectModule, MatOptionModule, NgFor, MatIconModule, MatInputModule, MatSlideToggleModule, NgIf, NgClass, MatTooltipModule, MatProgressBarModule, MatButtonModule, RouterLink, PercentPipe, I18nPluralPipe, AsyncPipe, MatSortModule, ReactiveFormsModule],

})
export class  ListComponent implements AfterViewInit, OnDestroy, OnInit {

    searchInputControl: UntypedFormControl = new UntypedFormControl();
    @ViewChild(MatPaginator) _paginator: MatPaginator;
    @ViewChild(MatSort) _sort: MatSort;

    displayedColumns: string[] = ['_id', 'name', 'category', 'Action'];

    pagination: Pagination;
    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    typeIncidents$: Observable<TypeIncident[]>;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _changeDetectorRef: ChangeDetectorRef,
        private _router: Router,
        private _liveAnnouncer: LiveAnnouncer,
        private _typeIncidentService: TypeIncidentService,
    ) {
    }

    ngOnInit() {
        this._typeIncidentService.pagination$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((pagination: any) => {
                this.pagination = pagination;
                console.log('pagination', pagination)
                this._changeDetectorRef.markForCheck();
            });

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                switchMap(query =>
                    // Search
                    this._typeIncidentService.getTypeIncident(0, 10, 'creation_date', 'asc', query),
                ),
            )
            .subscribe();
        this.typeIncidents$ = this._typeIncidentService.typeIncidents$;
        console.log('col', this.typeIncidents$)
    }

    ngAfterViewInit(): void
    {
        if (this._sort && this._paginator) {
            console.log('changed')

            // Set the initial sort
            this._sort.sort({
                id          : 'name',
                start       : 'asc',
                disableClear: true,
            });

            this._changeDetectorRef.markForCheck();

            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() =>
                {
                    // Reset back to the first page
                    this._paginator.pageIndex = 1;
                });


        }
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.id || index;
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() =>
        {
            this.flashMessage = null;

            // Mark for check
            this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    /** Announce the change in sort state for assistive technology. */
    announceSortChange(sortState: Sort) {
        // This example uses English messages. If your application supports
        // multiple language, you would internationalize these strings.
        // Furthermore, you can customize the message to add additional
        // details about the values being sorted.
        if (sortState.direction) {
            this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
        } else {
            this._liveAnnouncer.announce('Sorting cleared');
        }
    }
}
