export interface TypeIncident {
    name?: string;
    categoryId?: string;
    responsableId?: string;
}
